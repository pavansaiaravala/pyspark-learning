from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import col, monotonically_increasing_id, when, expr
from lib.logger import Log4j
from lib.utils import *

if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .appName("Misc Demo") \
        .master("local[2]") \
        .getOrCreate()

    logger = Log4j(spark)

    data_list = [("Pavan", "28", "1", "2002"),
                 ("Sai", "23", "5", "81"),  # 1981
                 ("Aravala", "12", "12", "6"),  # 2006
                 ("Smith", "7", "8", "63"),  # 1963
                 ("John", "23", "5", "81")]  # 1981
    
    raw_df=spark.createDataFrame(data_list).toDF("Name", "Day", "Month", "Year").repartition(3)
    raw_df.printSchema()

    df1=raw_df.withColumn("id", monotonically_increasing_id()) \
                .withColumn("Day", col("Day").cast(IntegerType())) \
                .withColumn("Month", col("Month").cast(IntegerType())) \
                .withColumn("Year", col("Year").cast(IntegerType())) \
                .withColumn("Year", when(col("Year") < 20, col("year") + 2000)
                .when(col("Year") < 100, col("Year") + 1900)
                .otherwise(col("Year"))) \
                .withColumn("dob", expr("to_date(concat(Day,'/',Month,'/',Year), 'd/M/y')")) \
                .drop("Day", "Month", "Year") \
                .dropDuplicates(["Name", "dob"]) \
                .sort(col("dob").desc()) \
                .show()
    
    spark.stop()
    

                               


