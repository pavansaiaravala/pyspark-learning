import re
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from lib.logger import Log4j
from lib.utils import *

#os.environ["spark.yarn.app.container.log.dir"]=os.path.dirname("C:/Users/290032838/Docs/AE/pyspark/")
#os.environ["logfile.name"]="hello-spark"

def parse_gender(gender):
    female_pattern = r"^f$|f.m|w.m"
    male_pattern = r"^m$|ma|m.l"

    if re.search(female_pattern, gender.lower()):
        return "Female"
    
    elif re.search(male_pattern, gender.lower()):
        return "Male"
    
    else:
        return "Unknown"

if __name__=='__main__':
    
    conf=get_spark_app_config()
    spark = SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate()
            
    logger=Log4j(spark)

    survey_df=spark.read \
                .format("csv") \
                .option("header", "true") \
                .option("inferSchema", "true") \
                .load("survey.csv")
    
    #survey_df.show(10)

    logger.info("Catalog Entry:")
    [logger.info(r) for r in spark.catalog.listFunctions() if r.name == "parse_gender_udf" ]

    parse_gender_udf = udf(parse_gender)
    survey_df_2 = survey_df.withColumn("Gender", parse_gender_udf("Gender")).show(10)

    spark.udf.register("parse_gender_udf", parse_gender)
    logger.info("Catalog Entry:")
    [logger.info(r) for r in spark.catalog.listFunctions() if r.name == "parse_gender_udf" ]

    survey_df_3=survey_df.withColumn("Gender", expr("parse_gender_udf(Gender)")).show(10)
    spark.stop()
    