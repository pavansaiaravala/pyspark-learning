import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import spark_partition_id
from lib.logger import Log4j
from lib.utils import *

#os.environ["spark.yarn.app.container.log.dir"]=os.path.dirname("C:/Users/290032838/Docs/AE/pyspark/")
#os.environ["logfile.name"]="hello-spark"

if __name__=='__main__':
    
   spark=SparkSession \
         .builder \
         .appName("spark datasink") \
         .master("local[3]") \
         .getOrCreate()
   
   logger=Log4j(spark)

   flightTImeParquetDF=spark.read \
                    .format("parquet") \
                    .load("flight*parquet")
   
   logger.info("num of partitions before = " + str(flightTImeParquetDF.rdd.getNumPartitions()))
   flightTImeParquetDF.groupBy(spark_partition_id()).count().show()

   partitionedDF=flightTImeParquetDF.repartition(5)
   logger.info("num of partitions after = " + str(partitionedDF.rdd.getNumPartitions()))
   partitionedDF.groupBy(spark_partition_id()).count().show()

   partitionedDF.write \
                .format("avro") \
                .mode("overwrite") \
                .option("path", "avro") \
                .save()
   
   partitionedDF.write \
                  .format("json") \
                  .mode("overwrite") \
                  .option("path", "json") \
                  .partitionBy("OP_CARRIER", "ORIGIN") \
                  .option("maxRecordsPerFile", 10000) \
                  .save()



