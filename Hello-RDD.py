from pyspark.sql import SparkSession
from lib.logger import Log4j
from lib.utils import *
from collections import namedtuple
import sys

SurveyRecord = namedtuple("SurveyRecord", ["Age", "Gender", "Country", "State"])


if __name__=='__main__':
    
    conf=get_spark_app_config()

    spark = SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate()
            
    
    logger=Log4j(spark)

    if len(sys.argv) != 2:
        logger.error("Usage: HelloSpark <filename>")
        sys.exit(-1)

   

    sc=spark.sparkContext
    lines_RDD=sc.textFile(sys.argv[1])
    partitioned_rdd=lines_RDD.repartition(2)
    cols_RDD=partitioned_rdd.map(lambda line: line.replace('"', '').split(","))
    select_RDD=cols_RDD.map(lambda cols: SurveyRecord(int(cols[1]), cols[2], cols[3], cols[4]))
    filter_RDD=select_RDD.filter(lambda row: row.Age < 40)
    keyval_RDD=filter_RDD.map(lambda row: (row.Country ,1))
    count_RDD=keyval_RDD.reduceByKey(lambda val1, val2: val1 + val2)

    result=count_RDD.collect()

    for columns in result:
        logger.info(columns)

    sc.stop()



