from pyspark.sql import SparkSession

from lib.logger import Log4j

if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .appName("Bucket join Demo") \
        .enableHiveSupport() \
        .master("local[3]") \
        .getOrCreate()

    logger = Log4j(spark)

    df1=spark.read.json("data/d1/")
    df2=spark.read.json("data/d2/")

    #df1.show()
    #df2.show()

    '''
    spark.sql("CREATE DATABASE IF NOT EXISTS my_db")
    spark.sql("USE my_db")

    df1.coalesce(1).write \
        .bucketBy(3, "id") \
        .mode("overwrite") \
        .saveAsTable("my_db.flight_data1")
    
    df2.coalesce(1).write \
        .bucketBy(3, "id") \
        .mode("overwrite") \
        .saveAsTable("my_db.flight_data2")
    
    '''
    spark.sql("SHOW TABLES")
    df3 = spark.read.table('my_db.flight_data1')
    df4 = spark.read.table('my_db.flight_data2')
    
    spark.conf.set("spark.sql.autoBroadCastThreshold", -1)

    join_expr = df3.id == df4.id
    join_df= df3.join(df4, join_expr, "inner") 

    join_df.collect()
    join_df.show()

