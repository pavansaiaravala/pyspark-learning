import sys
from pyspark.sql import SparkSession
from lib.logger import Log4j
from lib.utils import *

#os.environ["spark.yarn.app.container.log.dir"]=os.path.dirname("C:/Users/290032838/Docs/AE/pyspark/")
#os.environ["logfile.name"]="hello-spark"

if __name__=='__main__':
    
    conf=get_spark_app_config()
    spark = SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate()
            
    logger=Log4j(spark)
    logger.info("Starting hello spark")
    
    survey_df=load_survey_df(spark, sys.argv[1])
    partitioned_survey_df=survey_df.repartition(2)
    count_by_country_df=count_by_country(survey_df)
    count_by_country_df.show()
    
    logger.info("Finished hello spark")
    spark.stop()

    

    