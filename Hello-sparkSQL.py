import os
import sys
from pyspark.sql import SparkSession
from lib.logger import Log4j
from lib.utils import *

#os.environ["spark.yarn.app.container.log.dir"]=os.path.dirname("C:/Users/290032838/Docs/AE/pyspark/")
#os.environ["logfile.name"]="hello-spark"

if __name__=='__main__':
    
    conf=get_spark_app_config()
    spark = SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate()
            
    logger=Log4j(spark)
    logger.info("Starting hello spark")
    
    survey_df=load_survey_df(spark, sys.argv[1])
    survey_df.createOrReplaceTempView("survey_table")
    table=spark.sql("select Country, count(1) as count from survey_table where Age<40 group by Country")
    table.show()
    logger.info("Finished hello spark")
    spark.stop()
