from pyspark.sql import SparkSession
from pyspark.sql.functions import spark_partition_id
from lib.logger import Log4j
from lib.utils import *

#os.environ["spark.yarn.app.container.log.dir"]=os.path.dirname("C:/Users/290032838/Docs/AE/pyspark/")
#os.environ["logfile.name"]="hello-spark"

if __name__=='__main__':
    
   spark=SparkSession \
         .builder \
         .appName("spark sql table") \
         .master("local[3]") \
         .enableHiveSupport() \
         .getOrCreate()
   
   logger=Log4j(spark)

   flightTImeParquetDF=spark.read \
                    .format("parquet") \
                    .load("flight*parquet")
   
   spark.sql("CREATE DATABASE IF NOT EXISTS AIRLINE_DB")
   spark.catalog.setCurrentDatabase("AIRLINE_DB")

   
   flightTImeParquetDF.write \
                  .mode("overwrite") \
                  .bucketBy(5,"OP_CARRIER", "ORIGIN") \
                  .saveAsTable("flight_data_tbl")
   
   logger.info(spark.catalog.listTables("AIRLINE_DB"))
   spark.stop()
                  