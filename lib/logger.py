class Log4j(object):
    def __init__(self, spark):

        root_class = "pavan.spark.examples"
        log4j = spark._jvm.org.apache.log4j
        conf=spark.sparkContext.getConf()
        app_name = conf.get("spark.app.name")
        self.logger = log4j.LogManager.getLogger(root_class+"."+app_name)


    
    def warn(self, message):
        self.logger.warn(message)

    def error(self, message):
        self.logger.error(message)
                          
    def info(self, message):
        self.logger.info(message)

