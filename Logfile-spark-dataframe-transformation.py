from pyspark.sql import SparkSession
from lib.utils import *
from lib.logger import Log4j
from pyspark.sql.functions import regexp_extract, substring_index

if __name__=='__main__':
    
    conf=get_spark_app_config()
    spark = SparkSession.builder \
            .config(conf=conf) \
            .getOrCreate()
            
    logger=Log4j(spark)
    logger.info("Apache webserver log file reading")

    file_df=spark.read.text("apache_logs.txt")
    file_df.printSchema()

    reg_exp = r'^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+) (\S+) (\S+)" (\d{3}) (\S+) "(\S+)" "([^"]*)'

    logs_df=file_df.select(regexp_extract('value', reg_exp, 1).alias("ip"),
                           regexp_extract('value', reg_exp, 4).alias("Date"),
                           regexp_extract('value', reg_exp, 6).alias("request"),
                           regexp_extract('value', reg_exp, 10).alias("referrer"))
    logs_df \
        .where("trim(referrer) != '-' ")\
        .withColumn("referrer", substring_index("referrer", "/", 3)) \
        .show(100, truncate=False)

logs_df.printSchema()
