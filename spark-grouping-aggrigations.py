from pyspark.sql import SparkSession
import pyspark.sql.functions as f
from lib.logger import Log4j

if __name__ == "__main__":

    spark = SparkSession.builder \
            .appName("aggregations-demo") \
            .master("local[3]") \
            .getOrCreate()
    
    logger=Log4j(spark)

    invoice_df=spark.read \
                .format("csv") \
                .option("header", "true") \
                .option("inferSchema", "true") \
                .load("invoices.csv")
    
    NumbInvoices=f.countDistinct("InvoiceNo").alias("NumInvoices")
    TotalQuantity=f.sum("Quantity").alias("TotalQuantity")
    InvoiceValue=f.round(f.sum(f.expr("Quantity * UnitPrice")),2).alias("InvoiceValue")
    
    exSummary_df=invoice_df \
                .withColumn("InvoiceDate", f.to_date(f.col("InvoiceDate"), "dd-MM-yyyy H.mm")) \
                .where("year(InvoiceDate) == 2010") \
                .withColumn("WeekNumber", f.weekofyear(f.col("InvoiceDate"))) \
                .groupBy("Country", "WeekNumber") \
                .agg(NumbInvoices, TotalQuantity, InvoiceValue)
    
    exSummary_df.coalesce(1) \
                .write \
                .format("parquet") \
                .mode("overwrite") \
                .save("output")
    
    exSummary_df.sort("Country", "WeekNumber").show()